﻿using Kloud.Model;
using Kloud.Service.Interfaces;
using Kloud.WebApp.Helper;
using Kloud.WebApp.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Mvc;

namespace Kloud.WebApp.Controllers
{
    public class HomeController : BaseController
    {
        private ICarService _carService;

        static string baseAddress = ConfigurationManager.AppSettings["BaseAddress"];
        static string path = ConfigurationManager.AppSettings["Path"];

        public HomeController(ICarService carService)
        {
            this._carService = carService;
        }
        public ActionResult Index()
        {
            ViewBag.Title = string.Format(ViewBag.Title, "Home Page");

            List<CarsViewModel> cars = (from c in _carService.GetAll()
                                        orderby c.Brand, c.Colour
                                        group c by c.Brand into g
                                        select new CarsViewModel
                                        {
                                            Brand = g.Key,
                                            Owners = g.Select(b => new OwnerViewModel
                                            {
                                                Name = b.Owner.Name,
                                                CarColour = b.Colour
                                            }).ToList()
                                        }).ToList();

            //ViewBag.Cars = cars.ToList();
            Owner[] ownerList = RunAsync();

            ViewBag.Cars = (from owner in ownerList
                            from car in owner.Cars
                            orderby car.Brand, car.Colour
                            group owner by new { car.Brand } into list
                            select new CarsViewModel
                            {
                                Brand = list.Key.Brand,
                                Owners = list.Select(b => new OwnerViewModel
                                {
                                    Name = b.Name,
                                    CarColour = string.Empty
                                }).ToList()
                            }).ToList();

            return View();
        }

        static Owner[] RunAsync()
        {
            string ownerList = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync(path).Result;
                if (response.IsSuccessStatusCode)
                {
                    ownerList = response.Content.ReadAsStringAsync().Result;
                }
            }
            return Deserialize.FromJson(ownerList);
        }
    }

    public partial class Deserialize
    {
        public static Owner[] FromJson(string json) => JsonConvert.DeserializeObject<Owner[]>(json, Converter.SerializerSettings);
    }

    public partial class CarApi
    {
        public static List<Owner> FromJson(string json) => JsonConvert.DeserializeObject<List<Owner>>(json, Converter.SerializerSettings);
    }

}
