﻿using Kloud.Data.Repositories;
using Kloud.Data.Setup;
using Kloud.Model;
using Kloud.Service;
using Kloud.Service.Interfaces;
using Kloud.WebApp.Helper;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Kloud.WebApp.Controllers
{
    public class CarsController : ApiController
    {
        private IOwnerService _ownerService;

        public CarsController(IOwnerService ownerService)
        {
            this._ownerService = ownerService;
        }

        // GET api/cars
        public object Get()
        {
            var jsonSerialiser = new JavaScriptSerializer();

            var owners = _ownerService.GetAll().Select(c => new {
                name = c.Name,
                cars = c.Cars.Select(a => new {
                    brand = a.Brand,
                    colour = a.Colour
                })
            });

            if (owners.Count() > 0)
            {
                string json = JsonConvert.SerializeObject(owners, Converter.SerializerSettings);

                return JsonConvert.DeserializeObject(json);
            }
            else
            {
                return string.Empty;
            }
        }

        // GET api/cars/1
        public object GetOwner(int id)
        {
            var owner = _ownerService.GetAll().Where(p => p.Id == id).FirstOrDefault();

            if (owner == null)
            {
                return string.Empty;
            }

            string json = JsonConvert.SerializeObject(owner, Converter.SerializerSettings);

            return JsonConvert.DeserializeObject(json);
        }
    }
}
