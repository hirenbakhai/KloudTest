﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kloud.WebApp.ViewModels
{
    public class OwnerViewModel
    {
        public string Name { get; set; }

        public string CarColour { get; set; }
    }
}