﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kloud.WebApp.ViewModels
{
    public class CarsViewModel
    {
        public string Brand { get; set; }

        public List<OwnerViewModel> Owners { get; set; }
    }
}