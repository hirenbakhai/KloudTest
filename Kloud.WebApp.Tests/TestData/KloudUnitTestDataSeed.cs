﻿using Kloud.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kloud.WebApp.Tests.TestData
{
    public static class KloudUnitTestDataSeed
    {
        public static List<Owner> GetOwners()
        {
            return new List<Owner>
            {
                new Owner { Id = 1, Name = "Bradley",
                    Cars = new List<Car> {
                        new Car { Id = 1, Brand = "MG", Colour = "Blue", OwnerId = 1 }
                    }
                },
                new Owner { Id = 2, Name = "Demetrios",
                    Cars = new List<Car> {
                        new Car { Id = 2, Brand = "Toyota", Colour = "Green", OwnerId = 2 },
                        new Car { Id = 3, Brand = "Holden", Colour = "Blue", OwnerId = 2 }
                    }
                },
                new Owner { Id = 3, Name = "Brooke",
                    Cars = new List<Car> {
                        new Car { Id = 4, Brand = "Holden", Colour = "", OwnerId = 3 }
                    }
                },
                new Owner { Id = 4, Name = "Kristin",
                    Cars = new List<Car> {
                        new Car { Id = 5, Brand = "Toyota", Colour = "Blue", OwnerId = 4 },
                        new Car { Id = 6, Brand = "Mercedes", Colour = "Green", OwnerId = 4 },
                        new Car { Id = 7, Brand = "Mercedes", Colour = "Yellow", OwnerId = 4 }
                    }
                },
                new Owner { Id = 5, Name = "Andre",
                    Cars = new List<Car> {
                        new Car { Id = 8, Brand = "BMW", Colour = "Green", OwnerId = 5 },
                        new Car { Id = 9, Brand = "Holden", Colour = "Black", OwnerId = 5 }
                    }
                 },
                new Owner { Id = 6, Name = null,
                    Cars = new List<Car> {
                        new Car { Id = 10, Brand = "Mercedes", Colour = "Blue", OwnerId = 6 }
                    }
                },
                new Owner { Id = 7, Name = string.Empty,
                    Cars = new List<Car> {
                        new Car { Id = 11, Brand = "Mercedes", Colour = "Red", OwnerId = 7 },
                        new Car { Id = 12, Brand = "Mercedes", Colour = "Blue", OwnerId = 7 }
                    }
                },
                new Owner { Id = 8, Name = "Matilda",
                    Cars = new List<Car> {
                        new Car { Id = 13, Brand = "Holden", Colour = null, OwnerId = 8 },
                        new Car { Id = 14, Brand = "BMW", Colour = "Black", OwnerId = 8 }
                    }
                },
                new Owner { Id = 9, Name = "Iva",
                    Cars = new List<Car> {
                        new Car { Id = 15, Brand = "Toyota", Colour = "Purple", OwnerId = 9 }
                    }
                },
                new Owner { Id = 10, Name = null,
                    Cars = new List<Car> {
                        new Car { Id = 16, Brand = "Toyota", Colour = "Blue", OwnerId = 10 },
                        new Car { Id = 17, Brand = "Mercedes", Colour = "Blue", OwnerId = 10 }
                    }
                },
            };
        }

        public static List<Owner> EmptyOwnerList()
        {
            return new List<Owner> { };
        }

        public static List<Car> GetCars()
        {
            return new List<Car>
            {
                new Car { Id = 1, Brand = "MG", Colour = "Blue", OwnerId = 1, Owner = new Owner { Id = 1, Name = "Test" } },
                new Car { Id = 2, Brand = "Toyota", Colour = "Green", OwnerId = 2, Owner = new Owner { Id = 2, Name = "Demetrios" } },
                new Car { Id = 3, Brand = "Holden", Colour = "Blue", OwnerId = 2, Owner = new Owner { Id = 2, Name = "Demetrios" } },
                new Car { Id = 4, Brand = "Holden", Colour = "", OwnerId = 3, Owner = new Owner { Id = 3, Name = "Brooke" } },
                new Car { Id = 5, Brand = "Toyota", Colour = "Blue", OwnerId = 4, Owner = new Owner { Id = 4, Name = "Kristin" } },
                new Car { Id = 6, Brand = "Mercedes", Colour = "Green", OwnerId = 4, Owner = new Owner { Id = 4, Name = "Kristin" } },
                new Car { Id = 7, Brand = "Mercedes", Colour = "Yellow", OwnerId = 4, Owner = new Owner { Id = 4, Name = "Kristin" } },
                new Car { Id = 8, Brand = "BMW", Colour = "Green", OwnerId = 5, Owner = new Owner { Id = 5, Name = "Andre" } },
                new Car { Id = 9, Brand = "Holden", Colour = "Black", OwnerId = 5, Owner = new Owner { Id = 5, Name = "Andre" } },
                new Car { Id = 10, Brand = "Mercedes", Colour = "Blue", OwnerId = 6, Owner = new Owner { Id = 6, Name = null } },
                new Car { Id = 11, Brand = "Mercedes", Colour = "Red", OwnerId = 7, Owner = new Owner { Id = 7, Name = string.Empty } },
                new Car { Id = 12, Brand = "Mercedes", Colour = "Blue", OwnerId = 7, Owner = new Owner { Id = 7, Name = string.Empty } },
                new Car { Id = 13, Brand = "Holden", Colour = null, OwnerId = 8, Owner = new Owner { Id = 8, Name = "Matilda" } },
                new Car { Id = 14, Brand = "BMW", Colour = "Black", OwnerId = 8, Owner = new Owner { Id = 8, Name = "Matilda" } },
                new Car { Id = 15, Brand = "Toyota", Colour = "Purple", OwnerId = 9, Owner = new Owner { Id = 9, Name = "Iva" } },
                new Car { Id = 16, Brand = "Toyota", Colour = "Blue", OwnerId = 10, Owner = new Owner { Id = 10, Name = null } },
                new Car { Id = 17, Brand = "Mercedes", Colour = "Blue", OwnerId = 10, Owner = new Owner { Id = 10, Name = null } }
            };
        }
    }
}
