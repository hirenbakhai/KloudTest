﻿using Kloud.Service.Interfaces;
using Kloud.WebApp.Controllers;
using Kloud.WebApp.Tests.TestData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Web.Mvc;

namespace Kloud.WebApp.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            var expectedCars = KloudUnitTestDataSeed.GetCars();
            var carService = new Mock<ICarService>();
            carService.Setup(m => m.GetAll()).Returns(expectedCars);

            // Act
            HomeController controller = new HomeController(carService.Object);
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.AreEqual(expectedCars.GroupBy(c => c.Brand).Count(), result.ViewBag.Cars.Count);
            Assert.AreEqual("Home Page", result.ViewBag.Title);
        }
    }
}
