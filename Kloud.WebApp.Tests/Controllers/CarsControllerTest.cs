﻿using Kloud.Service.Interfaces;
using Kloud.WebApp.Controllers;
using Kloud.WebApp.Helper;
using Kloud.WebApp.Tests.TestData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;

namespace Kloud.WebApp.Tests.Controllers
{
    [TestClass]
    public class CarsControllerTest
    {
        Mock<IOwnerService> ownerService = new Mock<IOwnerService>();
        
        public object GetOwner() {
            StringBuilder owner2 = new StringBuilder();
            owner2.Append("[").AppendLine();
            owner2.Append("  {").AppendLine();
            owner2.Append("    \"name\": \"Demetrios\",").AppendLine();
            owner2.Append("    \"cars\": [").AppendLine();
            owner2.Append("      {").AppendLine();
            owner2.Append("        \"brand\": \"Toyota\",").AppendLine();
            owner2.Append("        \"colour\": \"Green\"").AppendLine();
            owner2.Append("      },").AppendLine();
            owner2.Append("      {").AppendLine();
            owner2.Append("        \"brand\": \"Holden\",").AppendLine();
            owner2.Append("        \"colour\": \"Blue\"").AppendLine();
            owner2.Append("      }").AppendLine();
            owner2.Append("    ]").AppendLine();
            owner2.Append("  }").AppendLine();
            owner2.Append("]");

            string output = owner2.ToString();

            string json = JsonConvert.SerializeObject(output, Converter.SerializerSettings);
            var expectedOutput = JsonConvert.DeserializeObject(json);

            return expectedOutput;
        }

        [TestMethod]
        public void Get()
        {
            var expectedOutput = GetOwner();

            // Arrange
            var expectedOwners = KloudUnitTestDataSeed.GetOwners().Where(c => c.Id == 2);
            ownerService.Setup(m => m.GetAll()).Returns(expectedOwners);

            // Arrange
            CarsController carController = new CarsController(ownerService.Object);

            // Act

            // if your action returns: Ok()
            var actionResult = carController.Get();

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(object));
            Assert.AreEqual(expectedOutput.ToString(), actionResult.ToString());
        }

        [TestMethod]
        public void GetOwner_ShouldReturnCorrectOwner()
        {
            // Arrange
            var owners = KloudUnitTestDataSeed.GetOwners();

            var expectedOwners = owners.Where(c => c.Id == 2).FirstOrDefault();
            string json = JsonConvert.SerializeObject(expectedOwners, Converter.SerializerSettings);
            var expectedOutput = JsonConvert.DeserializeObject(json);
            
            ownerService.Setup(m => m.GetAll()).Returns(owners);

            // Arrange
            CarsController carController = new CarsController(ownerService.Object);

            // Act

            // if your action returns: NotFound()
            var actionResult = carController.GetOwner(2);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(object));
            Assert.AreEqual(expectedOutput.ToString(), actionResult.ToString());
        }

        [TestMethod]
        public void GetEmptyOwnerList()
        {
            // Arrange
            var carController = new CarsController(ownerService.Object);

            // Act
            var expectedOwners = KloudUnitTestDataSeed.EmptyOwnerList();
            ownerService.Setup(m => m.GetAll()).Returns(expectedOwners);

            // if your action returns: Ok()
            object actionResult = carController.Get();
            Assert.IsInstanceOfType(actionResult, typeof(object));

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(object));
            Assert.AreEqual(string.Empty, actionResult.ToString());
        }

        [TestMethod]
        public void GetOwner_ShouldNotFindOwner()
        {
            var controller = new CarsController(ownerService.Object);

            var result = controller.GetOwner(999);
            Assert.IsInstanceOfType(result, typeof(string));
        }
    }
}
