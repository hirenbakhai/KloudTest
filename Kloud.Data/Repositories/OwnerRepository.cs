﻿using Kloud.Data.Setup;
using Kloud.Model;
using Kloud.Model.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Kloud.Data.Repositories
{
    public class OwnerRepository : RepositoryBase<Owner>, IOwnerRepository
    {
        private IQueryable<Owner> QueryWithIncludes { get { return this.Query.Include(m => m.Cars); } }

        public OwnerRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public override IEnumerable<Owner> GetAll()
        {
            return this.QueryWithIncludes.ToList();
        }
    }
}
