﻿using Kloud.Data.Setup;
using Kloud.Model;
using Kloud.Model.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Kloud.Data.Repositories
{
    public class CarRepository : RepositoryBase<Car>, ICarRepository
    {
        private IQueryable<Car> QueryWithIncludes { get { return this.Query.Include(m => m.Owner); } }

        public CarRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public override IEnumerable<Car> GetAll()
        {
            return this.QueryWithIncludes.ToList();
        }
    }
}
