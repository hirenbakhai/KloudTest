﻿using Kloud.Data.Mapping;
using Kloud.Data.Setup;
using Kloud.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kloud.Data
{
    public class KloudEntities : DbContext, IKloudUnitTestEntities
    {
        public KloudEntities(string connectionStringName) : base(connectionStringName) {
            
        }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CarMapping());
            modelBuilder.Configurations.Add(new OwnerMapping());
        }

        #region IKloudUnitTestEntities

        public DbSet<Car> Cars { get; set; }

        public DbSet<Owner> Owners { get; set; } 

        #endregion
    }
}
