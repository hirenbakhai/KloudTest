﻿using Kloud.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kloud.Data.Mapping
{
    public class OwnerMapping : EntityTypeConfiguration<Owner>
    {
        public OwnerMapping()
        {
            this.ToTable("owner");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("owner_id");
            this.Property(t => t.Name).HasColumnName("name");
        }
    }
}
