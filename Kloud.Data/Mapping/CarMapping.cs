﻿using Kloud.Model;
using System.Data.Entity.ModelConfiguration;

namespace Kloud.Data.Mapping
{
    public class CarMapping : EntityTypeConfiguration<Car>
    {
        public CarMapping()
        {
            this.ToTable("car");
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("car_id");
            this.Property(t => t.Brand).HasColumnName("brand");
            this.Property(t => t.Colour).HasColumnName("colour");

            this.Property(t => t.OwnerId).HasColumnName("owner_id");
        }
    }
}
