﻿using Kloud.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kloud.Data.Setup
{
    public interface IKloudUnitTestEntities
    {
        DbSet<Car> Cars { get; set; }

        DbSet<Owner> Owners { get; set; }
    }
}