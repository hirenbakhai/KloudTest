﻿using Kloud.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Kloud.Data.Setup
{
    public class RepositoryBase<TEntity> : IRepository<TEntity> where TEntity : class
    {
        #region Properties
        private KloudEntities dataContext;
        private readonly IDbSet<TEntity> dbSet;

        public IQueryable<TEntity> Query { get { return this.DbContext.Set<TEntity>().AsNoTracking(); } }

        protected IDbFactory DbFactory
        {
            get;
            private set;
        }

        protected KloudEntities DbContext
        {
            get { return dataContext ?? (dataContext = DbFactory.Init()); }
        }
        #endregion

        public RepositoryBase(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
            this.DbContext.Configuration.LazyLoadingEnabled = false;
            this.DbContext.Configuration.ProxyCreationEnabled = false;
            dbSet = DbContext.Set<TEntity>();
        }

        #region Implementation
        
        public virtual IEnumerable<TEntity> GetAll()
        {
            return dbSet.ToList();
        }

        #endregion

    }
}
