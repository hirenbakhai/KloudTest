﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kloud.Data.Setup
{
    public class DbFactory : Disposable, IDbFactory
    {
        KloudEntities dbContext;

        private string _connectionStringName = "KloudEntities";
        public string ConnectionStringName { get { return _connectionStringName; } }

        public KloudEntities Init()
        {
            return dbContext ?? (dbContext = new KloudEntities(ConnectionStringName));
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
