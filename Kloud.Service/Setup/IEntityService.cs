﻿using Kloud.Model.Setup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Kloud.Services.Setup
{
    public interface IEntityService<TEntity> where TEntity : BaseEntity
    {
        IEnumerable<TEntity> GetAll();
    }
}
