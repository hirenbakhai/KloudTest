﻿using Kloud.Model.Interfaces;
using Kloud.Model.Setup;
using Kloud.Services.Setup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Kloud.Service.Setup
{

    public abstract class EntityService<TEntity> : IEntityService<TEntity> where TEntity : BaseEntity
    {
        IUnitOfWork _unitOfWork;
        IRepository<TEntity> _repository;

        public EntityService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public EntityService(IUnitOfWork unitOfWork, IRepository<TEntity> repository)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return _unitOfWork.Repository<TEntity>().GetAll();
        }
    }
}
