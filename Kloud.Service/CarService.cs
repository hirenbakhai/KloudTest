﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Kloud.Model;
using Kloud.Services.Setup;
using Kloud.Service.Setup;
using Kloud.Service.Interfaces;
using Kloud.Model.Interfaces;
using Kloud.Model.Setup;

namespace Kloud.Service
{
    public class CarService : EntityService<Car>, ICarService
    {
        ICarRepository _carRepository;

        public CarService(IUnitOfWork unitOfWork, ICarRepository carRepository) : base(unitOfWork, carRepository)
        {
            _carRepository = carRepository;
        }

        public override IEnumerable<Car> GetAll()
        {
            return _carRepository.GetAll();
        }
    }
}
