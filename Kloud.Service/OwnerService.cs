﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Kloud.Model;
using Kloud.Services.Setup;
using Kloud.Service.Setup;
using Kloud.Service.Interfaces;
using Kloud.Model.Interfaces;
using Kloud.Model.Setup;

namespace Kloud.Service
{
    public class OwnerService : EntityService<Owner>, IOwnerService
    {
        IOwnerRepository _ownerRepository;

        public OwnerService(IUnitOfWork unitOfWork, IOwnerRepository ownerRepository) : base(unitOfWork, ownerRepository)
        {
            _ownerRepository = ownerRepository;
        }

        public override IEnumerable<Owner> GetAll()
        {
            return _ownerRepository.GetAll();
        }
    }
}
