﻿using Kloud.Model;
using Kloud.Services.Setup;

namespace Kloud.Service.Interfaces
{
    public interface IOwnerService : IEntityService<Owner>
    {
    }
}
