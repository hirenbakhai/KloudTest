﻿using Kloud.Model.Setup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kloud.Model
{
    public class Owner : Entity<int>
    {
        public string Name { get; set; }

        public IList<Car> Cars { get; set; }
    }
}