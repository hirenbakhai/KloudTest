﻿using Kloud.Model.Setup;

namespace Kloud.Model
{
    public class Car : Entity<int>
    {
        public string Brand { get; set; }

        public string Colour { get; set; }

        public int? OwnerId { get; set; }

        public Owner Owner { get; set; }
    }
}