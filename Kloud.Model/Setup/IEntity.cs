﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kloud.Model.Setup
{
    public interface IEntity<T>
    {
        T Id { get; set; }
    }
}
