﻿using Kloud.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kloud.Model.Setup
{
    public interface IUnitOfWork
    {
        IRepository<T> Repository<T>() where T : class;

        void Commit();
    }
}
