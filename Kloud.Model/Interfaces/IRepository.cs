﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kloud.Model.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        // Gets all entities of type TEntity
        IEnumerable<TEntity> GetAll();
    }
}
