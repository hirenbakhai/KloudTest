﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kloud.Model.Interfaces
{
    public interface IOwnerRepository : IRepository<Owner>
    {

    }
}
